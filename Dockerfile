FROM node:12-stretch AS builder

# Create app directory
WORKDIR /opt/global_auth
RUN mkdir ./tmp

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./
RUN npm ci --only=production


FROM node:12-slim
WORKDIR /opt/global_auth
RUN mkdir ./tmp
ENV TYPEORM_ENTITIES="./build/api/models/**/*.js"
ENV TYPEORM_CACHE="true"
ENV TYPEORM_MIGRATIONS="./build/migration/*.js"
#RUN npm install
# If you are building your code for production
COPY --from=builder /opt/global_auth/node_modules ./node_modules
COPY ./build ./build
COPY ./src/api/views ./build/api/views
COPY package*.json ./

EXPOSE 3000
CMD ["npm", "start"]
