# Api Skeleton

## Migrations
* Generating migrations  
    `typeorm migration:generate -n <name>`
* Creating migration  
    `typeorm migration:create -n <name>`
### Running migrations
First make sure that the migrations have been compiled. `npm run build` or `tsc`.  
* To run all migrations, that have not yet been run  
    `npm run migrateUp` 
* To revert the last migration  
    `npm run migrateDown`
    * To go back more than one step you have to run this command multiple times
