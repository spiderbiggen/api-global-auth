import {MigrationInterface, QueryRunner} from 'typeorm';

export class Plugins1574811302425 implements MigrationInterface {
  name = 'Plugins1574811302425';

  public async up(queryRunner: QueryRunner): Promise<any> {
    // language=PostgreSQL
    await queryRunner.query('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    // language=PostgreSQL
    await queryRunner.query('DROP EXTENSION IF EXISTS "uuid-ossp";');
  }

}
