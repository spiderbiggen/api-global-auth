import {MigrationInterface, QueryRunner} from 'typeorm';

export class DefaultUser1574811302427 implements MigrationInterface {
  name = 'DefaultUser1574811302427';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      // language=PostgreSQL
        `INSERT INTO "user"("id", "created_at", "updated_at", "username", "email", "password", "roles")
         VALUES (DEFAULT, DEFAULT, DEFAULT, $1, $2, $3, $4)
         RETURNING "id", "created_at", "updated_at"`,
      [
        'admin',
        'webmaster@spiderbiggen.com',
        '$2b$14$ioY0b6FgpILncmwJMGE1V.SMsBz/W7uerqnHRbLpOFxhgrbvRluES',
        ['admin']
      ]
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      // language=PostgreSQL
        `DELETE
         FROM "user"
         where username = $1`,
      [
        'admin'
      ]
    );
  }

}
