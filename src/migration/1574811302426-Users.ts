import { MigrationInterface, QueryRunner } from 'typeorm';

export class Users1574811302426 implements MigrationInterface {
  name = 'Users1574811302426';

  public async up(queryRunner: QueryRunner): Promise<any> {
    // language=PostgreSQL
    await queryRunner.query(
        `CREATE TABLE "user"
         (
           "id"         UUID                     NOT NULL DEFAULT uuid_generate_v4(),
           "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
           "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
           "username"   CHARACTER VARYING        NOT NULL,
           "email"      CHARACTER VARYING        NOT NULL,
           "password"   CHARACTER VARYING        NOT NULL,
           "roles"      text                     NOT NULL,
           CONSTRAINT "UQ_055984372b62f71f274f3fe361" UNIQUE ("username"),
           CONSTRAINT "UQ_a1689164dbbcca860ce6d17b2e" UNIQUE ("email"),
           PRIMARY KEY ("id")
         )`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    // language=PostgreSQL
    await queryRunner.query('DROP TABLE "user"', undefined);
  }

}
