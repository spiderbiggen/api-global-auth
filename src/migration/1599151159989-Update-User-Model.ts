import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateUserModel1599151159989 implements MigrationInterface {
    name = 'UpdateUserModel1599151159989'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "user" ADD "google" character varying`, undefined);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "password" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "roles" DROP NOT NULL`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "roles" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "password" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "google"`, undefined);
    }

}
