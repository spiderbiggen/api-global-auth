import {MigrationInterface, QueryRunner} from 'typeorm';

export class TypeormCache1574811302423 implements MigrationInterface {
  name = 'TypeormCache1574811302423';

  public async up(queryRunner: QueryRunner): Promise<any> {
    // language=PostgreSQL
    await queryRunner.query(`CREATE TABLE "query-result-cache" ("id" SERIAL NOT NULL, "identifier" character varying, "time" bigint NOT NULL, "duration" integer NOT NULL, "query" text NOT NULL, "result" text NOT NULL, CONSTRAINT "PK_6a98f758d8bfd010e7e10ffd3d3" PRIMARY KEY ("id"))`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    // language=PostgreSQL
    await queryRunner.query(`DROP TABLE "query-result-cache"`, undefined);
  }

}
