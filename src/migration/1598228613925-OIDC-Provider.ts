import {MigrationInterface, QueryRunner} from "typeorm";

export class OIDCProvider1598228613925 implements MigrationInterface {
    name = 'OIDCProvider1598228613925'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "access_token" ("id" character varying NOT NULL, "data" jsonb NOT NULL, "expiresAt" TIMESTAMP, "consumedAt" TIMESTAMP, "grantId" character varying NOT NULL, CONSTRAINT "PK_f20f028607b2603deabd8182d12" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "authorization_code" ("id" character varying NOT NULL, "data" jsonb NOT NULL, "expiresAt" TIMESTAMP, "consumedAt" TIMESTAMP, "grantId" character varying NOT NULL, CONSTRAINT "PK_586233caf7e281dc24aaedd1335" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "client" ("id" character varying NOT NULL, "data" jsonb NOT NULL, "expiresAt" TIMESTAMP, "consumedAt" TIMESTAMP, CONSTRAINT "PK_96da49381769303a6515a8785c7" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "client_credentials" ("id" character varying NOT NULL, "data" jsonb NOT NULL, "expiresAt" TIMESTAMP, "consumedAt" TIMESTAMP, CONSTRAINT "PK_abd8de48912b01e7ba58724272f" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "device_code" ("id" character varying NOT NULL, "data" jsonb NOT NULL, "expiresAt" TIMESTAMP, "consumedAt" TIMESTAMP, "grantId" character varying NOT NULL, "userCode" character varying NOT NULL, CONSTRAINT "PK_3a62a161670ed87b6720f511f8b" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "session" ("id" character varying NOT NULL, "data" jsonb NOT NULL, "expiresAt" TIMESTAMP, "consumedAt" TIMESTAMP, "uid" character varying NOT NULL, CONSTRAINT "PK_f55da76ac1c3ac420f444d2ff11" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "refresh_token" ("id" character varying NOT NULL, "data" jsonb NOT NULL, "expiresAt" TIMESTAMP, "consumedAt" TIMESTAMP, "grantId" character varying NOT NULL, CONSTRAINT "PK_b575dd3c21fb0831013c909e7fe" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "initial_access_token" ("id" character varying NOT NULL, "data" jsonb NOT NULL, "expiresAt" TIMESTAMP, "consumedAt" TIMESTAMP, CONSTRAINT "PK_8126ae80d2c4d6fdb8b6360866a" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "registration_access_token" ("id" character varying NOT NULL, "data" jsonb NOT NULL, "expiresAt" TIMESTAMP, "consumedAt" TIMESTAMP, CONSTRAINT "PK_417a02f0eecd3df3c4b4800d551" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "interaction" ("id" character varying NOT NULL, "data" jsonb NOT NULL, "expiresAt" TIMESTAMP, "consumedAt" TIMESTAMP, CONSTRAINT "PK_9204371ccb2c9dab5428b406413" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "replay_detection" ("id" character varying NOT NULL, "data" jsonb NOT NULL, "expiresAt" TIMESTAMP, "consumedAt" TIMESTAMP, CONSTRAINT "PK_a5d55fc192f0eeb95396017cf34" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "pushed_authorization_request" ("id" character varying NOT NULL, "data" jsonb NOT NULL, "expiresAt" TIMESTAMP, "consumedAt" TIMESTAMP, CONSTRAINT "PK_a5baf698ac5e36bcf23c57a4557" PRIMARY KEY ("id"))`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE "pushed_authorization_request"`, undefined);
        await queryRunner.query(`DROP TABLE "replay_detection"`, undefined);
        await queryRunner.query(`DROP TABLE "interaction"`, undefined);
        await queryRunner.query(`DROP TABLE "registration_access_token"`, undefined);
        await queryRunner.query(`DROP TABLE "initial_access_token"`, undefined);
        await queryRunner.query(`DROP TABLE "refresh_token"`, undefined);
        await queryRunner.query(`DROP TABLE "session"`, undefined);
        await queryRunner.query(`DROP TABLE "device_code"`, undefined);
        await queryRunner.query(`DROP TABLE "client_credentials"`, undefined);
        await queryRunner.query(`DROP TABLE "client"`, undefined);
        await queryRunner.query(`DROP TABLE "authorization_code"`, undefined);
        await queryRunner.query(`DROP TABLE "access_token"`, undefined);
    }

}
