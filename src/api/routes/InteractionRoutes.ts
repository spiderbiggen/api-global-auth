import bodyParser from 'body-parser';
import { NextFunction, Request, Response } from 'express';
import {InteractionController} from '../controllers';
import {RouteWrapper} from '../../routing';

const router = new RouteWrapper('/interaction');

function setNoCache(req: Request, res: Response, next: NextFunction) {
  res.set('Pragma', 'no-cache');
  res.set('Cache-Control', 'no-cache, no-store');
  next();
}

router.registerMiddleware(setNoCache);

router.get('/:uid', InteractionController.render, false);

const parsedRouter = new RouteWrapper('/')
parsedRouter.registerMiddleware(bodyParser.urlencoded({ extended: false }))
parsedRouter.post('/:uid/login', InteractionController.login, false);
parsedRouter.post('/:uid/google', InteractionController.google, false);
parsedRouter.post('/:uid/confirm', InteractionController.confirm, false);
router.subRoutes(parsedRouter);


router.get('/:uid/abort', InteractionController.abort, false);

export default router;
