import InteractionRoutes from './InteractionRoutes';

/**
 * Export a list of all routes
 */
export default [InteractionRoutes];
