function onSignIn(googleUser) {
  const idToken = googleUser.getAuthResponse().id_token;
  console.log('ID Token: ' + idToken); // This is null if the 'email' scope is not present.
  const form = document.createElement('form');
  form.method = 'post';
  form.action = `/interaction/${uid}/google`;
  const idField = document.createElement('input');
  idField.type = 'hidden';
  idField.name = 'id_token';
  idField.value = idToken;
  form.appendChild(idField);
  document.body.appendChild(form);
  form.submit();
}
window.onbeforeunload = function () {
  gapi.auth2.getAuthInstance().signOut();
};
