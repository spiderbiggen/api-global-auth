import {CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn} from 'typeorm';

export abstract class KeyedEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
}

export abstract class DatedEntity extends KeyedEntity {
  @CreateDateColumn({type: 'timestamp with time zone'})
  created_at: Date;

  @CreateDateColumn({type: 'timestamp with time zone'})
  updated_at: Date;
}
