import { compare, hash } from 'bcrypt';
import md5 from 'md5';
import { KoaContextWithOIDC } from 'oidc-provider';
import { Column, Entity, getRepository } from 'typeorm';
import { DatedEntity } from './DatedEntity';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity('user')
export class User extends DatedEntity {

  @Column({ nullable: false, unique: true })
  username: string;
  @Column({ unique: true })
  email: string;
  @Column({ nullable: true, select: false })
  password?: string; // technically a hash of the password.
  @Column('simple-array', { nullable: true })
  roles?: string[];
  @Column({ nullable: true, select: false })
  google?: string;

  get avatar(): string {
    return `//www.gravatar.com/avatar/${ md5(this.email.toLowerCase()) }?d=identicon`;
  }

  static get repository() {
    return getRepository(User);
  }

  static async isAdmin(id?: string): Promise<boolean> {
    const user = await User.repository.findOne(id);
    return user && user.roles ? user.roles.includes('admin') : false;
  }

  static async authorize(email: string, password: string): Promise<User | null> {
    const user = await User.repository.findOne({ email: email.toLowerCase() }, { select: ['password'] });
    if (user && user.password && await compare(password, user.password)) {
      return await User.repository.findOne({ email: email.toLowerCase() }) ?? null;
    }
    return null;
  }

  static async createUser(username: string, email: string, password: string, roles ?: string[]): Promise<User> {
    const pass = await hash(password, 14);
    const repo = User.repository;
    const u = repo.create({username, password: pass, email: email.toLowerCase(), roles});
    const user = await repo.save(u);
    console.log(user);
    return user;
  }

  toJSON(): any {
    return {
      id: this.id,
      username: this.username,
      email: this.email,
      roles: this.roles,
      avatar: this.avatar,
    };
  }

  static async findAccount(ctx: KoaContextWithOIDC, id: string) {
        const user = await User.repository.findOne(id);
        if(!user) {
          throw new Error();
        }
        return {
          accountId: user.id,
          async claims() {
            return {
              sub: id,
              email_verified: true,
              ...user,
            }
          }
        }
  }
}
