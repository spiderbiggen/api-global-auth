import { Entity, getRepository } from 'typeorm';
import { AuthModel } from './AuthModel';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity('initial_access_token')
export class InitialAccessToken extends AuthModel {

  static get repository() {
    return getRepository(InitialAccessToken);
  }

}
