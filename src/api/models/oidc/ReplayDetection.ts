import { Entity, getRepository } from 'typeorm';
import { AuthModel } from './AuthModel';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity('replay_detection')
export class ReplayDetection extends AuthModel {

  static get repository() {
    return getRepository(ReplayDetection);
  }

}
