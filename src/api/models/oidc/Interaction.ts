import { Entity, getRepository } from 'typeorm';
import { AuthModel } from './AuthModel';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity('interaction')
export class Interaction extends AuthModel {

  static get repository() {
    return getRepository(Interaction);
  }

}
