import { Entity, getRepository } from 'typeorm';
import { AuthModel } from './AuthModel';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity('registration_access_token')
export class RegistrationAccessToken extends AuthModel {

  static get repository() {
    return getRepository(RegistrationAccessToken);
  }

}
