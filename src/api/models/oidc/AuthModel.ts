import { Column, PrimaryColumn } from 'typeorm';

export abstract class AuthModel {
  @PrimaryColumn()
  id: string;

  @Column('jsonb')
  data: object;

  @Column({ nullable: true })
  expiresAt: Date;

  @Column({ nullable: true })
  consumedAt?: Date;
}
