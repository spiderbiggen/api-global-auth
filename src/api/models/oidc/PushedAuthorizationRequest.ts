import { Entity, getRepository } from 'typeorm';
import { AuthModel } from './AuthModel';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity('pushed_authorization_request')
export class PushedAuthorizationRequest extends AuthModel {

  static get repository() {
    return getRepository(PushedAuthorizationRequest);
  }

}
