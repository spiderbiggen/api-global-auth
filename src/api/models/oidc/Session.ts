import { Column, Entity, getRepository } from 'typeorm';
import { AuthModel } from './AuthModel';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity('session')
export class Session extends AuthModel {

  @Column()
  uid: string;

  static get repository() {
    return getRepository(Session);
  }

}
