import { Column, Entity, getRepository } from 'typeorm';
import { AuthModel } from './AuthModel';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity('access_token')
export class AccessToken extends AuthModel {

  @Column()
  grantId: string;

  static get repository() {
    return getRepository(AccessToken);
  }

}
