import { Column, Entity, getRepository } from 'typeorm';
import { AuthModel } from './AuthModel';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity('authorization_code')
export class AuthorizationCode extends AuthModel {

  @Column()
  grantId: string;

  static get repository() {
    return getRepository(AuthorizationCode);
  }

}
