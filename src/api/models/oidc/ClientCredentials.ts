import { Entity, getRepository } from 'typeorm';
import { AuthModel } from './AuthModel';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity('client_credentials')
export class ClientCredentials extends AuthModel {

  static get repository() {
    return getRepository(ClientCredentials);
  }

}
