import { Column, Entity, getRepository } from 'typeorm';
import { AuthModel } from './AuthModel';

/**
 * Entity that holds information about a user.
 *
 * @author Stefan Breetveld
 */
@Entity('device_code')
export class DeviceCode extends AuthModel {

  @Column()
  grantId: string;

  @Column()
  userCode: string;

  static get repository() {
    return getRepository(DeviceCode);
  }

}
