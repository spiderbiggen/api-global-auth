import { NextFunction, Request, Response } from 'express';
import { OAuth2Client, TokenPayload } from 'google-auth-library';
import { AnyObject, PromptDetail } from 'oidc-provider';
import oidc from '../../util/oauth/OidcProvider';
import { User } from '../models';

const API_HOST = process.env.API_HOST ?? 'http://localhost:3000';

const GOOGLE_CLIENT_ID = process.env.GOOGLE_CLIENT_ID;
const GOOGLE_SECRET = process.env.GOOGLE_SECRET;

type OidcParams = { client?: AnyObject, uid: string, prompt: PromptDetail, params: AnyObject };
type LoginParams = { login_hint?: string, flash?: string, title?: string, googleClientID?: string, [key: string]: any };

export module InteractionController {
  export async function render(req: Request, res: Response, next: NextFunction) {
    try {
      const { params, prompt, uid } = await oidc.interactionDetails(req, res);

      const client = await oidc.Client.find(params.client_id);
      if (prompt.name === 'login') {
        return renderLogin(res, { client, uid, prompt, params });
      }

      return res.render('interaction', {
        client,
        uid,
        details: prompt.details,
        params,
        title: 'Authorize',
      });
    } catch (err) {
      return next(err);
    }
  }

  export async function login(req: Request, res: Response, next: NextFunction) {
    try {
      const { params, prompt, uid } = await oidc.interactionDetails(req, res);

      const account = await User.authorize(req.body.email, req.body.password);

      if (!account) {
        const client = await oidc.Client.find(params.client_id);
        return renderLogin(res,
          { client, uid, prompt, params },
          {
            login_hint: req.body.email,
            flash: 'Invalid email or password.',
          },
        );
      }

      const result = {
        login: {
          account: account.id,
        },
      };

      await oidc.interactionFinished(req, res, result, { mergeWithLastSubmission: false });
    } catch (err) {
      next(err);
    }
  }

  async function verifyGoogle(client: OAuth2Client, token: string): Promise<TokenPayload | null> {
    const ticket = await client.verifyIdToken({
      idToken: token,
      audience: GOOGLE_CLIENT_ID,
    });
    const payload = ticket.getPayload();
    if (!payload) return null;
    return payload;
  }

  export async function google(req: Request, res: Response, next: NextFunction) {
    try {
      const { params, prompt, uid } = await oidc.interactionDetails(req, res);

      const client = await oidc.Client.find(params.client_id);
      if (typeof req.body.id_token !== 'string') {
        return renderLogin(res,
          { client, uid, prompt, params },
          { flash: 'No Google Account.' },
        );
      }
      const googleClient = new OAuth2Client(GOOGLE_CLIENT_ID);
      const tokenPayload = await verifyGoogle(googleClient, req.body.id_token);
      const repository = User.repository;

      if (!tokenPayload) {
        return renderLogin(res,
          { client, uid, prompt, params },
          { flash: 'No Valid Google Account.' },
        );
      }
      let account = await repository.findOne({ where: { google: tokenPayload.sub } });
      if (!account && !await repository.findOne({ where: { email: tokenPayload.email } })) {
        const temp = repository.create({
          email: tokenPayload.email,
          username: tokenPayload.name,
          google: tokenPayload.sub,
        });
        account = await repository.save(temp);
      }
      console.log(account);

      if (!account) {
        return renderLogin(res,
          { client, uid, prompt, params },
          { flash: 'Invalid Google Account.' },
        );
      }

      const result = {
        login: {
          account: account.id,
        },
      };

      await oidc.interactionFinished(req, res, result, { mergeWithLastSubmission: false });
    } catch (err) {
      next(err);
    }
  }

  export async function confirm(req: Request, res: Response, next: NextFunction) {
    try {
      const result = {
        consent: {
          // rejectedScopes: [], // < uncomment and add rejections here
          // rejectedClaims: [], // < uncomment and add rejections here
        },
      };
      await oidc.interactionFinished(req, res, result, { mergeWithLastSubmission: true });
    } catch (err) {
      next(err);
    }
  }

  export async function abort(req: Request, res: Response, next: NextFunction) {
    try {
      const result = {
        error: 'access_denied',
        error_description: 'End-User aborted interaction',
      };
      await oidc.interactionFinished(req, res, result, { mergeWithLastSubmission: false });
    } catch (err) {
      next(err);
    }
  }

  function renderLogin(
    res: Response,
    { client, uid, prompt, params }: OidcParams,
    { login_hint, flash, title = 'Sign-in', ...data }: LoginParams = {},
  ) {
    console.log(client);
    res.render('login', {
      client,
      uid,
      details: prompt.details,
      params: {
        ...params,
        login_hint,
      },
      title,
      flash,
      googleClientID: GOOGLE_CLIENT_ID,
      ...data,
    });
  }
}
