import assert from 'assert';
import { randomBytes } from 'crypto';

// STATE LENGTH in bytes
const MAX_STATE_LENGTH = 117;
const MIN_STATE_LENGTH = 53;
assert(MAX_STATE_LENGTH >= MIN_STATE_LENGTH);
const DELTA_STATE_LENGTH = MAX_STATE_LENGTH - MIN_STATE_LENGTH;


const tagsToReplace: { [key: string]: string } = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
};

export function escape(text: string): string {
  const pattern = `[${ Object.keys(tagsToReplace).join() }]`;
  return text.replace(new RegExp(pattern, 'g'), tag => tagsToReplace[tag] ?? tag);
}

export function generateState(): string {
  return generateBase64Salt(Math.round(Math.random() * DELTA_STATE_LENGTH + MIN_STATE_LENGTH));
}

export function generateBase64Salt(length: number): string {
  return randomBytes(length).toString('base64');
}

export function toBoolean(value?: string | number): boolean {
  if (!value) {
    return false;
  }
  if (typeof value === 'string') {
    value = value.toLowerCase();
  }
  switch (value) {
    case 'y':
    case 'yes':
    case 't':
    case 'true':
    case '1':
      return true;
    default:
      return false;
  }
}
