import { Adapter, AdapterPayload } from 'oidc-provider';
import { EntityTarget, getRepository } from 'typeorm';
import {
  AccessToken,
  AuthorizationCode,
  Client,
  ClientCredentials,
  DeviceCode,
  InitialAccessToken,
  Interaction,
  PushedAuthorizationRequest,
  RefreshToken,
  RegistrationAccessToken,
  ReplayDetection,
  Session,
} from '../../api/models';

const models = new Map<String, EntityTarget<unknown>>([
  ['Session', Session],
  ['AccessToken', AccessToken],
  ['AuthorizationCode', AuthorizationCode],
  ['RefreshToken', RefreshToken],
  ['DeviceCode', DeviceCode],
  ['ClientCredentials', ClientCredentials],
  ['Client', Client],
  ['InitialAccessToken', InitialAccessToken],
  ['RegistrationAccessToken', RegistrationAccessToken],
  ['Interaction', Interaction],
  ['ReplayDetection', ReplayDetection],
  ['PushedAuthorizationRequest', PushedAuthorizationRequest],
]);

export class TypeOrmAdapter implements Adapter {
  name: string;
  model: EntityTarget<unknown>;

  constructor(name: string) {
    const model = models.get(name);
    if (!model) throw new Error();
    this.model = model;
    this.name = name;
  }

  private get repository() {
    return getRepository(this.model);
  }

  async consume(id: string): Promise<void> {
    await this.repository.update({ consumedAt: new Date() }, { id });
  }

  async destroy(id: string): Promise<void> {
    await this.repository.delete({ id });
  }

  async find(id: string): Promise<AdapterPayload | void | undefined> {
    const found: any = await this.repository.findOne(id);
    if (!found) return undefined;
    return {
      ...found.data,
      ...(found.consumedAt ? { consumed: true } : undefined),
    };
  }

  async findByUid(uid: string): Promise<AdapterPayload | void | undefined> {
    const found: any = await this.repository.findOne({ uid });
    if (!found) return undefined;
    return {
      ...found.data,
      ...(found.consumedAt ? { consumed: true } : undefined),
    };
  }

  async findByUserCode(userCode: string): Promise<AdapterPayload | void | undefined> {
    const found: any = await this.repository.findOne({ userCode });
    if (!found) return undefined;
    return {
      ...found.data,
      ...(found.consumedAt ? { consumed: true } : undefined),
    };
  }

  async revokeByGrantId(grantId: string): Promise<void | undefined> {
    await this.repository.delete({ grantId });
  }

  async upsert(id: string, data: AdapterPayload, expiresIn: number): Promise<void> {
    await this.repository.save({
      id,
      data,
      ...(data.grantId ? { grantId: data.grantId } : undefined),
      ...(data.userCode ? { userCode: data.userCode } : undefined),
      ...(data.uid ? { uid: data.uid } : undefined),
      ...(expiresIn ? { expiresAt: new Date(Date.now() + (expiresIn * 1000)) } : undefined),
    });
  }

}
