import assert from 'assert';
import Provider from 'oidc-provider';
import { User } from '../../api/models';
import { TypeOrmAdapter } from './TypeOrmAdapter';

const COOKIE_KEYS = process.env.COOKIE_KEYS;
assert(COOKIE_KEYS, 'Cookie keys need to be set');
const cookieKeys = COOKIE_KEYS.split(',');
assert(cookieKeys.length >= 2, 'Make sure cookie keys is comma separated list of at least 2 keys.')

const jwks = require('../../../jwks.json');

const oidc = new Provider(`https://auth.spiderbiggen.com`, {
  adapter: TypeOrmAdapter,
  findAccount: User.findAccount,
  jwks,
  claims: {
    openid: ['sub'],
    email: ['email', 'email_verified'],
    profile: ['username', 'avatar'],
    auth: ['roles'],
  },
  interactions: {
    url(ctx) {
      return `/interaction/${ ctx.oidc.uid }`;
    },
  },
  features: {
    devInteractions: { enabled: false },
    encryption: { enabled: true },
    clientCredentials: { enabled: true },
    sessionManagement: { enabled: false },
    introspection: { enabled: true },
    revocation: { enabled: true },
  },
  cookies: {
    keys: cookieKeys,
  },
});

oidc.proxy = true;

if (process.env.NODE_ENV === 'development') {
// @ts-ignore
  const { invalidate: orig } = oidc.Client.Schema.prototype;

// @ts-ignore
  oidc.Client.Schema.prototype.invalidate = function invalidate(message, code) {
    if (code === 'implicit-force-https' || code === 'implicit-forbid-localhost') {
      return;
    }

    orig.call(this, message);
  };
}

export default oidc;
