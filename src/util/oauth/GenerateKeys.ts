import fs from 'fs';
import jose from 'jose';
import path from 'path';

const filePath = path.resolve('./jwks.json');

if (!fs.existsSync(filePath)) {
  const keystore = new jose.JWKS.KeyStore();

  Promise.all([
    keystore.generate('RSA', 2048, { use: 'sig' }),
    keystore.generate('RSA', 2048, { use: 'enc' }),
    keystore.generate('EC', 'P-256', { use: 'sig' }),
    keystore.generate('EC', 'P-256', { use: 'enc' }),
    keystore.generate('OKP', 'Ed25519', { use: 'sig' }),
  ]).then(() => {
    fs.writeFileSync(filePath, JSON.stringify(keystore.toJWKS(true), null, 2));
    console.log('done');
  });
}
