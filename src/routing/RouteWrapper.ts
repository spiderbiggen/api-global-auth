import {Express, NextFunction, Request, Response, Router} from 'express';
import {RequestHandler, RequestHandlerParams} from 'express-serve-static-core';
import {GenericServerError, HttpError, ResponseHelper} from '../util';

const prodEnv = process.env.NODE_ENV == 'production';

/**
 * Wrap the express router in a custom class for easier error handling and sending responses.
 *
 * @author Stefan Breetveld
 */
export class RouteWrapper {
  private readonly router: Router;

  /**
   * construct a new instance with a relative path.
   *
   * @param path relative path to this resource
   */
  constructor(private path: string) {
    this.router = Router();
  }

  /**
   * Register middleware on the router to intercept incoming traffic.
   *
   * @param middleware the middleware function used to intercept and/or relay the request
   * @param path the relative path this middleware should act on
   */
  registerMiddleware(middleware: (req: Request, res: Response, next: NextFunction) => any, path: string = '*') {
    this.router.use(path, middleware);
  }

  /**
   * HTTP GET. Accept get requests on the given route and handle them with the given handler.
   *
   * @param route the relative route for this resource
   * @param handler the handler that processes the request
   * @param autoResponseHandling Should the router handle sending the response to the client.
   */
  get(route: string, handler: RequestHandler, autoResponseHandling: boolean = true) {
    this.router.route(route).get(autoResponseHandling ?  this.createRouteHandler(handler): handler);
  }

  /**
   * HTTP POST. Accept post requests on the given route and handle them with the given handler.
   *
   * @param route the relative route for this resource
   * @param handler the handler that processes the request
   * @param autoResponseHandling Should the router handle sending the response to the client.
   */
  post(route: string, handler: RequestHandler, autoResponseHandling: boolean = true) {
    this.router.route(route).post(autoResponseHandling ?  this.createRouteHandler(handler): handler);
  }

  /**
   * HTTP PUT. Accept put requests on the given route and handle them with the given handler.
   *
   * @param route the relative route for this resource
   * @param handler the handler that processes the request
   * @param autoResponseHandling Should the router handle sending the response to the client.
   */
  put(route: string, handler: RequestHandler, autoResponseHandling: boolean = true) {
    this.router.route(route).put(autoResponseHandling ?  this.createRouteHandler(handler): handler);
  }

  /**
   * HTTP DELETE. Accept delete requests on the given route and handle them with the given handler.
   *
   * @param route the relative route for this resource
   * @param handler the handler that processes the request
   * @param autoResponseHandling Should the router handle sending the response to the client.
   */
  delete(route: string, handler: RequestHandler, autoResponseHandling: boolean = true) {
    this.router.route(route).delete(autoResponseHandling ?  this.createRouteHandler(handler): handler);
  }

  /**
   * Add the given routeWrapper as sub routes of this RouteWrapper.
   *
   * @param routeWrapper a routeWrapper with sub routes.
   */
  subRoutes(routeWrapper: RouteWrapper) {
    this.router.use(routeWrapper.path, routeWrapper.router);
  }

  /**
   * Register this router to the given express app on the correct sub route.
   *
   * @param app the express app.
   * @param root the root of your endpoints
   */
  registerRoutes(app: Express, root: string = '') {
    app.use(`${root}${this.path}`, this.router);
  }

  /**
   * Convert a Handler into a method that can be used by express. Any errors will be handled here.
   *
   * @param handler the method that will process the requested resource.
   */
  private createRouteHandler(handler: RequestHandler): RequestHandlerParams {
    return async (req: Request, res: Response, next: NextFunction) => {
      handler(req, res, next)
        .then((result: any) => ResponseHelper.sendResponse(req, res, result))
        .catch((e: any) => {
          if (!(e instanceof HttpError)) {
            e = prodEnv ? new GenericServerError() : new GenericServerError({error: e});
          }
          ResponseHelper.sendError(req, res, e);
        });
    };
  }
}
