import assert from 'assert';
import compression from 'compression';
import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import path from 'path';
import 'reflect-metadata';
import { createConnection } from 'typeorm';
import routes from './api/routes';
import { ROUTE_REGISTRY } from './routing';
import { Authentication, LOGGER } from './util';
import oidc from './util/oauth/OidcProvider';

const root = process.env.NODE_ROOT || '';
const port = process.env.PORT || 3000;
const prodEnv = process.env.NODE_ENV === 'production';
const sessionSecret = process.env.SESSION_SECRET;

assert(sessionSecret && sessionSecret.length > 16, 'SESSION_SECRET must be set');

/**
 * Create a database connection then load all routes. Then create the server.
 *
 * @author Stefan Breetveld
 */
createConnection()
  .then(() => ROUTE_REGISTRY.load(routes))
  .then(() => {
    const app = express();
    app.use(compression());
    app.use(helmet({
      contentSecurityPolicy: {
        directives: {
          defaultSrc: ["'self'", "https://auth.spiderbiggen.com"],
          scriptSrc: ["'self'", "'unsafe-inline'", "https://spiderbiggen.com", "https://apis.google.com", "https://accounts.google.com"],
          styleSrc: ["'self'", "'unsafe-inline'", "https://spiderbiggen.com", "https://use.fontawesome.com", "https://fonts.googleapis.com", "https://accounts.google.com"],
          connectSrc: ["'self'", "https://accounts.google.com"],
          fontSrc: ["'self'", "https://fonts.gstatic.com", "https://use.fontawesome.com"],
          objectSrc: ["'none'"],
          frameSrc: ["'self'", "https://accounts.google.com"],
          upgradeInsecureRequests: [],
        }
      }
    }));
    app.use(cors());
    // Get User from token if available, for all routes
    app.use(Authentication.userMiddleware);

    app.set('json spaces', prodEnv ? 0 : 2);
    app.set('trust proxy', 1);
    app.set('view engine', 'pug');
    app.set('views', path.resolve(__dirname, 'api', 'views'));

    ROUTE_REGISTRY.registerRoutes(app, root);
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(oidc.callback);

    app.listen(port);
    LOGGER.info('API Running on :%s/%s', port, root ?? '');
  }).catch(err => {
  LOGGER.error(err);
});

